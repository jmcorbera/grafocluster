package interfaz;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;

import javax.swing.JComboBox;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;

import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class MainForm extends JFrame {
	private JPanel contentPane;
	private JMapViewer _mapa;
	private JMap jmap;

	@SuppressWarnings("rawtypes")
	private JComboBox cmbInstancias;
	private JTextField txtNroClusters;
	private JButton btnGenerarAgm;
	private JButton btnGenerarCluster;
	private JButton btnRefrescar;
	private JButton btnGrafoCompleto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm frame = new MainForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();		
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes" })
	private void initialize() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		_mapa = new JMapViewer();
		_mapa.setBounds(10, 11, 805, 539);
		contentPane.add(_mapa);
		_mapa.setDisplayPosition(new Coordinate(-34.5217, -58.7006), 15);
		
		this.jmap = new JMap(this._mapa);

		JPanel panelMapa = new JPanel();
		panelMapa.setBounds(0, 0, 1008, 561);
		contentPane.add(panelMapa);
		panelMapa.setLayout(null);

		cmbInstancias = new JComboBox();
		cmbInstancias.setToolTipText("Seleccionar Instancia para cargar al mapa");
		cmbInstancias.setBounds(820, 11, 140, 25);
		panelMapa.add(cmbInstancias);

		btnGrafoCompleto = new JButton("Grafo Completo");
		btnGrafoCompleto.setBounds(820, 47, 178, 27);
		panelMapa.add(btnGrafoCompleto);

		btnGenerarAgm = new JButton("Generar AGM");
		btnGenerarAgm.setBounds(820, 85, 178, 27);
		panelMapa.add(btnGenerarAgm);

		txtNroClusters = new JTextField();
		txtNroClusters.setBounds(948, 136, 50, 27);
		panelMapa.add(txtNroClusters);
		txtNroClusters.setColumns(10);

		btnGenerarCluster = new JButton("Generar Cluster");
		btnGenerarCluster.setBounds(820, 169, 178, 27);
		panelMapa.add(btnGenerarCluster);
		
		JLabel lblNroClusters = new JLabel("Nro. de Clusters :");
		lblNroClusters.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNroClusters.setBounds(820, 136, 118, 27);
		panelMapa.add(lblNroClusters);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(820, 123, 178, 2);
		panelMapa.add(separator);
		
		btnRefrescar = new JButton(new ImageIcon("./img/refrescar.png"));
		btnRefrescar.setBounds(961, 11, 37, 25);
		panelMapa.add(btnRefrescar);

		cmbInstancias.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {	
				limpiarControles();
				
				if (cmbInstancias.getSelectedIndex() != -1) {
					btnRefrescar.setEnabled(true);
					btnGrafoCompleto.setEnabled(true);
					btnGenerarAgm.setEnabled(true);
					dibujar();
				}
			}
		});
		
		btnGrafoCompleto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jmap.dibujarGrafoCompleto();
			}
		});

		btnGenerarAgm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				txtNroClusters.setEnabled(true);
				jmap.dibujarGrafoAGM();
			}
		});
		
		btnGenerarCluster.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int clusters = Integer.parseInt(txtNroClusters.getText());
				try
				{
					jmap.dibujarGrafoAGMcluster(clusters);
				}
				catch(IllegalArgumentException e)
				{
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});
		
		txtNroClusters.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(txtNroClusters.getText().isEmpty())
				{
					btnGenerarCluster.setEnabled(false);
				}
				else
				{
					btnGenerarCluster.setEnabled(true);
				}
			}
		});
		
		btnRefrescar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				jmap.limpiarMapa();
				inicializar();
			}
		});
		
		inicializar();
	}

	private void dibujar()
	{
		String instancia = cmbInstancias.getSelectedItem().toString() + ".txt";
		this.jmap.dibujarVectores(instancia); 
	}	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void inicializar() 
	{
		try
		{
			cmbInstancias.setModel(new DefaultComboBoxModel(jmap.obtenerInstancias()));
			cmbInstancias.setSelectedIndex(-1);
		}
		catch(FileNotFoundException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		jmap.limpiarMapa();
		limpiarControles();
	}
	
	private void limpiarControles()
	{
		btnGenerarAgm.setEnabled(false);
		btnGenerarCluster.setEnabled(false);
		btnGrafoCompleto.setEnabled(false);
		btnRefrescar.setEnabled(false);
		txtNroClusters.setEnabled(false);	
		txtNroClusters.setText("");
	}
}
