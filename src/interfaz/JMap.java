package interfaz;

import grafoCluster.Arista;
import grafoCluster.GrafoCluster;
import grafoCluster.GrafoP;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

public class JMap {
    private JMapViewer mapa;
    private GrafoCluster grafoCluster;
	
    public JMap(JMapViewer mapa) {  	
    	this.mapa = mapa; 	
    	this.grafoCluster = new GrafoCluster();
    }
    
    public Object[] obtenerInstancias() throws FileNotFoundException
    {
    	return grafoCluster.ObtenerIntancias().toArray(); 	
    }
      
    public void dibujarVectores(String instancia) 
    {  	
    	limpiarMapa();
    	
    	try {
    		GrafoP grafoP = grafoCluster.crearGrafo(instancia);
        	
    		for (Coordinate vector : grafoP.obtenerCoordenadas()) {
    			agregarPunto(vector);
    		}		
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
    }
    
    public void dibujarGrafoCompleto()
    {
    	GrafoP p = this.grafoCluster.obtenerGrafoCompleto();
    	dibujarAristas(p);
    }
    
    public void dibujarGrafoAGM()
    {  	 	
   	    GrafoP p = this.grafoCluster.obtenerGrafoAGM();
   	    dibujarAristas(p);
    } 
    
    public void dibujarGrafoAGMcluster(int cluster)
    {  	
   	    GrafoP p = this.grafoCluster.obtenerGrafoCluster(cluster);   	    
   	    dibujarAristas(p);
    }
    
	public void limpiarMapa() {
		this.mapa.removeAllMapMarkers();
		this.mapa.removeAllMapPolygons();
	}
    
	private void dibujarAristas(GrafoP p) 
	{
		this.mapa.removeAllMapPolygons();	
    	for(Arista arista : p.obtenerAristas())
    	{
    		agregarArista(arista);
    	}
	}
	
	private void agregarPunto(Coordinate vector) 
	{
		MapMarkerDot marker = new MapMarkerDot(vector);
		marker.getStyle().setBackColor(Color.GREEN);
		this.mapa.addMapMarker(marker);
	}
	
	private void agregarArista(Arista arista) {
		List<MapMarker> mapMarkers = this.mapa.getMapMarkerList();
		ArrayList<Coordinate> coordenadas = new ArrayList<>();
		
		coordenadas.add(mapMarkers.get(arista.getVertice1()).getCoordinate());
		coordenadas.add(mapMarkers.get(arista.getVertice2()).getCoordinate());
		coordenadas.add(mapMarkers.get(arista.getVertice1()).getCoordinate());

		MapPolygon polygon = new MapPolygonImpl(coordenadas);
		this.mapa.addMapPolygon(polygon);
	}
}
