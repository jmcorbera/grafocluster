package grafoCluster;

public class Arista implements Comparable<Arista>{
	private int vertice1;
	private int vertice2;
	private double peso;

	public Arista(int v1, int v2, double p)
	{
		this.vertice1=v1;
		this.vertice2=v2;
		this.peso=p;		
	}
	
	public int getVertice1()
	{
		return this.vertice1;
	}
	
	public int getVertice2()
	{
		return this.vertice2;
	}
	
	public double getPeso()
	{
		return this.peso;
	}

	@Override
	public int compareTo(Arista o) {

        int resultado=0;

        if (this.peso>o.peso) 
        {   
        	resultado = -1;
        }

        else if (this.peso<o.peso) 
        {
        	resultado = 1;
        }
		return resultado;       
	}
	
	@Override
	public boolean equals (Object o) {

        if (o instanceof Arista) {

            Arista arista= (Arista) o;

            if (arista.getVertice1()==this.getVertice1() &&
            	arista.getVertice2()==this.getVertice2() &&
            	arista.getPeso()==this.getPeso()) 
            	{
            		return true;
            	}
           
            
        }
        return false;
	}
}	
