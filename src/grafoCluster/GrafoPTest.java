package grafoCluster;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import grafoCluster.Assert;

public class GrafoPTest {
		
	@Test
	public void ordenarAristasTest() 		
	{
		GrafoP gp = generarAristas();
		gp.ordenarAristas();
		Assert.iguales(generarAristasOrdenadas(), gp.obtenerAristas());
	}
	
	@Test
	public void borrarAristaTest() 	
	{
		GrafoP gp = generarAristas();
		gp.borrarArista(0);
		assertFalse(gp.existeArista(new Arista(0, 1 , 1)));
	}
	
	@Test
	public void existeAristaTest()
	{
		GrafoP gp = generarAristas();
		assertTrue(gp.existeArista(gp.getArista(2)));
	}

	private GrafoP generarAristas() {
		GrafoP gp= new GrafoP(4);
		Arista a=new Arista(0, 1 , 1);
		Arista c=new Arista(0, 3 , 8);
		Arista b=new Arista(0, 2 , 4);
		gp.agregarArista(a.getVertice1(), a.getVertice2(), a.getPeso());
		gp.agregarArista(b.getVertice1(), b.getVertice2(), b.getPeso());
		gp.agregarArista(c.getVertice1(), c.getVertice2(), c.getPeso());
		return gp;
	}
	
	private ArrayList<Arista> generarAristasOrdenadas() {
		GrafoP gp= new GrafoP(4);
		Arista a=new Arista(0, 1 , 1);
		Arista b=new Arista(0, 2 , 4);
		Arista c=new Arista(0, 3 , 8);
		gp.agregarArista(a.getVertice1(), a.getVertice2(), a.getPeso());
		gp.agregarArista(b.getVertice1(), b.getVertice2(), b.getPeso());
		gp.agregarArista(c.getVertice1(), c.getVertice2(), c.getPeso());
		return gp.obtenerAristas();
	}
	
}
