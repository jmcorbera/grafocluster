package grafoCluster;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

public class GrafoClusterTest {	
	GrafoCluster grafoC; 
	AGM grafoAGM;
	
	@Test
	public void obtenerGrafoCompletoTest()
	{	
		CrearGrafo();
		
		GrafoP grafo = grafoC.obtenerGrafoCompleto();
		
		assertEquals(12, grafo.obtenerAristas().size());
	}
	
	@Test
	public void obtenerGrafoAGMTest()
	{	
		CrearGrafo();
		grafoC.obtenerGrafoCompleto();
		GrafoP grafoAgm = grafoC.obtenerGrafoAGM();
		
		assertEquals(3, grafoAgm.obtenerAristas().size());
	}
	
	@Test
	public void obtenerGrafoClusterTest()
	{	
		CrearGrafo();
		grafoC.obtenerGrafoCompleto();
		grafoC.obtenerGrafoAGM();
		
		GrafoP grafocl = grafoC.obtenerGrafoCluster(1);
		
		assertEquals(2, grafocl.obtenerAristas().size());
	}
	
	public void CrearGrafo()
	{		
		grafoC = new GrafoCluster();
		try
		{
			grafoC.crearGrafo("instanciaTest.txt");
		}
		catch(FileNotFoundException e)
		{}
	}
	
	
	
}
