package grafoCluster;

import static org.junit.Assert.*;

import org.junit.Test;


public class AristaTest {
	
	@Test
	public void compareToAristaTest() 
		
	{
	
	Arista a=new Arista(0, 1, 2);
	Arista b=new Arista(0, 3, 4);
	assertEquals(1, a.compareTo(b));
	
	}
	
	@Test
	public void igualesAristaTest() 		
	{
	
	Arista a=new Arista(0, 1, 2);
	Arista b=new Arista(0, 1, 2);
	assertTrue(a.equals(b));
	
	}
	
}
