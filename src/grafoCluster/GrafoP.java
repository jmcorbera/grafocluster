package grafoCluster;

import java.util.ArrayList;
import java.util.Collections;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class GrafoP {
	private Grafo grafo;
	private ArrayList<Arista> aristas; //aristas con pesos del grafo
	private ArrayList<Coordinate> coordenadas;
	
	public GrafoP(int n)
	{
		this.grafo = new Grafo(n);
		this.aristas = new ArrayList<Arista>();	
	}
	
	public void agregarCoordenadas(ArrayList<Coordinate> coordenadas)
	{
		this.coordenadas = coordenadas;
	}
	
	public ArrayList<Coordinate> obtenerCoordenadas()
	{
		return this.coordenadas;
	}
	
	public void agregarArista(int i, int j, double peso)
	{
		grafo.agregarArista(i,j);
		this.aristas.add(new Arista(i, j, peso));
	}
	
	public void borrarArista(int index)
	{
		Arista arista = this.aristas.get(index);
		grafo.borrarArista(arista.getVertice1(),arista.getVertice2());
		this.aristas.remove(index);
	}
	
	public int tamanio()
	{
		return this.coordenadas.size();
	}
	
	public double obtenerPeso(int i, int j)
	{
		return Math.sqrt(Math.pow(coordenadas.get(i).getLat()-coordenadas.get(j).getLat(), 2) + Math.pow(coordenadas.get(i).getLon()-coordenadas.get(j).getLon(),2));	
	}
	
	public ArrayList<Arista> obtenerAristas()
	{
		return this.aristas;
	}
	
	public void ordenarAristas() 
	{
		Collections.sort(this.aristas); 
	}
	
	public boolean existeArista(Arista a)
	{
		return this.aristas.contains(a);
	}
	
	public Arista getArista(int i)
	{
		return this.aristas.get(i);
	}
	
    @Override
	public String toString(){
    	StringBuilder str = new StringBuilder();
    	
    	for(Arista a : this.aristas)
    	{
    		str.append("Arista: [" + a.getVertice1() + "][" + a.getVertice2() + "], Peso = " + a.getPeso() + "\n"); 
    	}    	   	
		return str.toString();
	}
	
}