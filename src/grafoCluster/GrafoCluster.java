package grafoCluster;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class GrafoCluster {
	private GrafoP grafoP;
	private AGM grafoAGM;

	// Generar Arbol Completo
	public GrafoP obtenerGrafoCompleto()
	{	
		ArrayList<Coordinate> coordenadas = grafoP.obtenerCoordenadas();
		
		if(coordenadas != null)
		{
			for(int i=0; i<coordenadas.size(); ++i)
			{
				for(int j=0; j<coordenadas.size(); ++j)
				{
					if( i != j )
					{
						this.grafoP.agregarArista(i, j, calcularDistancia(coordenadas.get(i), coordenadas.get(j)));
					}			
				}
			}
		}
		
		return this.grafoP;
	}
	
	public GrafoP obtenerGrafoAGM()
	{
		this.grafoAGM = new AGM();
		
		GrafoP p = grafoAGM.obtenerAGM(grafoP);
		this.grafoAGM.ObtenerGrafo().ordenarAristas();
		
		return p;
	}

	public GrafoP obtenerGrafoCluster(int clusters)
	{
		if(this.grafoAGM.ObtenerGrafo().obtenerAristas().size() == 0)
			throw new IllegalArgumentException("Ya no Quedan aristas para eliminar");
		
		if(clusters > this.grafoAGM.ObtenerGrafo().obtenerAristas().size())
			throw new IllegalArgumentException("Se ha superado el tama�o de las aristas a eliminar");
		
		int i = 0;
		while (i < clusters) {			
			this.grafoAGM.ObtenerGrafo().borrarArista(0);
			i++;
		}

		return grafoAGM.ObtenerGrafo();
	}
	
	public ArrayList<String> ObtenerIntancias() 
	{
		ArrayList<String> ret = new ArrayList<String>();

		File archivo = new File(ObtenerPathRelativo() + "/inst");
		File[] archivos = archivo.listFiles();

		for (File file : archivos) {
			if (file.isFile()) {
				String name[] = file.getName().split("[.]");

				if (name[1].equals("txt")) {
					ret.add(name[0]);
				}
			}
		}
		return ret;
	}
	
	public GrafoP crearGrafo(String file) throws FileNotFoundException
	{
		FileInputStream fis = new FileInputStream(ObtenerPathRelativo() + "/inst/" + file);
		Scanner scanner = new Scanner(fis);

		ArrayList<Coordinate> vectores = new ArrayList<Coordinate>();
		vectores = new ArrayList<Coordinate>();
		
		while (scanner.hasNextLine()) {
			String text = scanner.nextLine().trim();
			if (!text.isEmpty()) {
				String coords[] = text.split(" ");	
				vectores.add(new Coordinate(Double.parseDouble(coords[0]), Double.parseDouble(coords[1])));
			}
		}
		
		scanner.close();		
		this.grafoP = new GrafoP(vectores.size());
		this.grafoP.agregarCoordenadas(vectores);
		
		return this.grafoP;
	}

	private double calcularDistancia(Coordinate p1, Coordinate p2) {
		return Math.sqrt(Math.pow((p2.getLat() - p1.getLat()), 2) +Math.pow((p2.getLon() - p1.getLon()), 2));
	}
	
	private String ObtenerPathRelativo()
	{
		File currDir = new File(".");
		String path = currDir.getAbsolutePath();
		String s = path.substring(0, path.length() - 2);
		
		return s;
	}
		

	
}
