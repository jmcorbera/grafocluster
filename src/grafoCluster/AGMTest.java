package grafoCluster;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;


public class AGMTest {
	private AGM _agm;
	private GrafoP _grafoG;
	private ArrayList<Coordinate> _coordenadas;
	
	@Before
	public void inicializar()
	{
		_agm=new AGM();		
		_grafoG = new GrafoP(4);		
		generarCoordenadas();		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafoNuloTest()
	{
		GrafoP grafo = null;
		_agm.obtenerAGM(grafo);
	}

	@Test
	public void obtenerAGM()
	{	
		_agm.obtenerAGM(_grafoG);
		GrafoP grafoTest = generarGrafoAGM();	
		assertEquals(grafoTest.toString(), _agm.ObtenerGrafo().toString());
	}
	
	@Test
	public void obtenerAGMCeroAristas()
	{	
		GrafoP grafo = new GrafoP(1);
		grafo.agregarCoordenadas(new ArrayList<Coordinate>());
		
		assertEquals(0, grafo.tamanio());
	}
	
	@Test
	public void obtenerAGMUnaAristas()
	{	
		GrafoP grafo = generarGrafoUnaArista();		
		_agm.obtenerAGM(grafo);
	
		assertEquals(grafo.toString(), _agm.ObtenerGrafo().toString());
	}
	
	private GrafoP generarGrafoUnaArista() {
		GrafoP _grafoT = new GrafoP(2);
		_coordenadas = new ArrayList<Coordinate>();
		_coordenadas.add(new Coordinate(-34.52186820666683, -58.70265483856201));
		_coordenadas.add(new Coordinate(-34.52133782929332, -58.70068073272705));
		_grafoT.agregarCoordenadas(_coordenadas);
		_grafoT.agregarArista(0, 1, calcularDistancia(_coordenadas.get(0), _coordenadas.get(1)));;
		return _grafoT;
	}
			
	private GrafoP generarGrafoAGM() {
		GrafoP _grafoT = new GrafoP(4);
		_grafoT.agregarCoordenadas(_coordenadas);
		_grafoT.agregarArista(0, 3, calcularDistancia(_coordenadas.get(0), _coordenadas.get(3)));
		_grafoT.agregarArista(3, 2, calcularDistancia(_coordenadas.get(3), _coordenadas.get(2)));
		_grafoT.agregarArista(2, 1, calcularDistancia(_coordenadas.get(2), _coordenadas.get(1)));
		
		return _grafoT;
	}
	
	private void generarCoordenadas() {
		_coordenadas = new ArrayList<Coordinate>();
		_coordenadas.add(new Coordinate(-34.52186820666683, -58.70265483856201));
		_coordenadas.add(new Coordinate(-34.52133782929332, -58.70068073272705));
		_coordenadas.add(new Coordinate(-34.520772089706036, -58.702311515808105));
		_coordenadas.add(new Coordinate(-34.52126711205503, -58.70325565338135));
		
		_grafoG.agregarCoordenadas(_coordenadas);
	}
	
	private double calcularDistancia(Coordinate p1, Coordinate p2) {
		// TODO Auto-generated method stub
		return Math.sqrt(Math.pow((p2.getLat() - p1.getLat()), 2) +Math.pow((p2.getLon() - p1.getLon()), 2));
	}
}
