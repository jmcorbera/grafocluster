package grafoCluster;

public class Grafo {
	int V; //vertices
	private boolean[][] A; //aristas
	
	public Grafo(int vertices)
	{
		this.V = vertices;
		A = new boolean[vertices][vertices];
	}
	
	// Operaciones sobre aristas
	public void agregarArista(int i, int j)
	{
		verificarIndices(i, j);
		A[i][j] = A[j][i] = true;
	}
	
	public void borrarArista(int i, int j)
	{
		verificarIndices(i, j);
		A[i][j] = A[j][i] = false;
	}
	
	public boolean existeArista(int i, int j)
	{
		verificarIndices(i, j);
		return A[i][j];
	}

	///////////////////////////////////
	// Chequeo de Vertices e indices //
	///////////////////////////////////
	
	// Lanza excepciones si los �ndices no son v�lidos
	private void verificarIndices(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		
		if( i == j )
			throw new IllegalArgumentException("No existen aristas entre un vertice y si mismo! vertice = " + i);
	}
	
	private void verificarVertice(int i)
	{
		if( i < 0 || i >= tamanio() )
			throw new IllegalArgumentException("El vertice " + i + " no existe!");
	}

	public int tamanio()
	{
		return A.length;
	}
	
	@Override
	public String toString(){
		String grafo = "";
		for(int i = 0; i < A.length; i++){
			grafo += "Vertice: " + i + " Arista: " + A[i].toString() + "\n";
		}
		return grafo;
	}

}
