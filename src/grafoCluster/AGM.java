package grafoCluster;

import java.util.HashSet;
import java.util.Set;

public class AGM {
	private GrafoP grafoP;
	private Set<Integer> amarillos;

	public GrafoP obtenerAGM(GrafoP grafoG) {	
		
		if(grafoG == null) 
			throw new IllegalArgumentException("El Grafo no pueder ser Nulo!");
		
		this.grafoP = new GrafoP(grafoG.tamanio());
		amarillos = new HashSet<>();
		amarillos.add(0);

		while (amarillos.size() < grafoG.tamanio()) {
			Arista menorArista = obtenerMenorArista(grafoG, amarillos);
			grafoP.agregarArista(menorArista.getVertice1(), menorArista.getVertice2(), menorArista.getPeso());
			amarillos.add(menorArista.getVertice2());
			System.out.print(menorArista.getVertice2() + "\n");
		}
		
		return grafoP;
	}

	public GrafoP ObtenerGrafo()
	{
		return grafoP;
	}
	
	private Arista obtenerMenorArista(GrafoP grafoG, Set<Integer> amarillos) {
		double pesoMenor = 0.0;
		Arista aristaMenor = null;
		
		for (Integer a : amarillos) {
			for (int j = 0; j < grafoG.tamanio() ; j++) {
				if (!amarillos.contains(j)) {
					if(pesoMenor == 0.0 || grafoG.obtenerPeso(a, j) < pesoMenor)
					{
						pesoMenor = grafoG.obtenerPeso(a,j);
						aristaMenor = new Arista(a,j,pesoMenor);
					}
				}
			}
		}
		
		return aristaMenor;
	}
	
}
