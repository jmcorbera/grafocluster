package grafoCluster;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

public class Assert {
	public static void iguales(ArrayList<Arista> expected, ArrayList<Arista> obtained)
	{
		assertEquals(expected.size(), obtained.size());
		
		for(Arista valor: expected)
			assertTrue( obtained.contains(valor) );
	}

}
